"use strict";

export class Customer {
  id: string;
  name: string;
  details: string;

  // more fields when necessary for persist in database
  // I prefer simple object for test

  constructor(id = "", name = "", details = "") {
    this.id = id;
    this.details = details;
    this.name = name;
  }
}
