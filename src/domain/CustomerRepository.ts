"use strict";

import { Customer } from "./Customer";

interface IListAllCustomers {
  data: Array<Customer>[];
}

interface IFilter {
  id: string;
  name: string;
  details: string;
}

export class CustomerRepository {
  // eslint-disable-next-line
  async create(payload: Customer): Promise<Customer> {
    throw new Error("ERROR CREATE CUSTOMER REPOSITORY");
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(name: string, id: string, details: string): Promise<Customer> {
    throw new Error("ERROR UPDATE CUSTOMER REPOSITORY");
  }

  async list(): Promise<IListAllCustomers> {
    throw new Error(`ERROR LIST CUSTOMER REPOSITORY`);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(filter: IFilter): Promise<Customer> {
    throw new Error("ERROR FIND CUSTOMER REPOSITORY");
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async delete(name: string, id: string): Promise<Customer> {
    throw new Error("ERROR FIND CUSTOMER REPOSITORY");
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async search(name: string, details: string): Promise<Customer> {
    throw new Error("ERROR SEARCH CUSTOMER REPOSITORY");
  }
}
