import Hapi from "@hapi/hapi";
import { Server } from "@hapi/hapi";

export let server: Server;

// import routes
import { customerRoutes, healthRoutes, securityRoutes, servercustomerRoutes } from "./interfaces/routes/Index";

import * as HapiSwagger from "hapi-swagger";
import * as Inert from "@hapi/inert";
import * as Vision from "@hapi/vision";

// Configuração Hapi
export const init = async function (): Promise<Server> {
  server = Hapi.server({
    port: process.env.PORT || 3000,
    host: process.env.SERVER || "localhost",
  });

  const swaggerOptions = {
    info: {
      title: "Pagaleve - Challenger API Documentation",
    },
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const plugins: Array<Hapi.ServerRegisterPluginObject<any>> = [
    {
      plugin: Inert,
    },
    {
      plugin: Vision,
    },
    {
      plugin: HapiSwagger,
      options: swaggerOptions,
    },
  ];

  await server.register(plugins);

  // Todo: refactor to index export file.
  server.route(healthRoutes);
  server.route(customerRoutes);
  server.route(securityRoutes);
  server.route(servercustomerRoutes);

  return server;
};

// Iniciando web server com HAPI
export const start = async function (): Promise<void> {
  console.log(`Server running: ${server.settings.host}- port: ${server.settings.port}`);
  return server.start();
};

process.on("unhandledRejection", (err) => {
  console.error("unhandledRejection");
  console.error(err);
  process.exit(1);
});
