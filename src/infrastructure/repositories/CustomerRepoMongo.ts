"use strict";

import { Customer } from "./../../domain/Customer";
import { CustomerRepository } from "./../../domain/CustomerRepository";
import { create, list, update, deleteCustomer, find } from "./../orm/mongo/schemas/Customer";

interface IListAllCustomers {
  data: Array<Customer>[];
}

interface IFilter {
  name: string;
  details: string;
}

export class CustomerRepoMongo extends CustomerRepository {
  constructor() {
    super();
  }

  // Todo: remove any Type for accept ICustomer and response Object
  async create(payload: Object): Promise<Customer> {
    try {
      const newCustomer = (await create(payload)) as Customer;

      return newCustomer;
    } catch (error) {
      throw new Error("Not success in create new Customer!");
    }
  }

  async find(filter: IFilter): Promise<Customer> {
    try {
      const findCustomer = await find(filter);

      return findCustomer as Customer;
    } catch (error) {
      throw new Error("Not success in create new Customer!");
    }
  }

  async list(): Promise<IListAllCustomers> {
    try {
      const ListAllCustomer = (await list()) as IListAllCustomers;

      return ListAllCustomer;
    } catch (error) {
      throw new Error("Not success in create new Customer!");
    }
  }

  async update(id: string, name: string): Promise<Customer> {
    try {
      const updateCustomer = (await update(id, name)) as Customer;

      return updateCustomer;
    } catch (error) {
      throw new Error("Not success in create new Customer!");
    }
  }

  async delete(id: string): Promise<Customer> {
    try {
      const deleteC = await deleteCustomer({ id });

      return deleteC as Customer;
    } catch (error) {
      throw new Error("Not success in create new Customer!");
    }
  }

  async search(name: string, details: string): Promise<Customer> {
    try {
      const searchCustomer = await find({ name, details });
      console.log(searchCustomer, "search");
      return searchCustomer as Customer;
    } catch (error) {
      throw new Error("Not success in create new Customer!");
    }
  }
}
