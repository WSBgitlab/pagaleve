"use strict";

import * as dotenv from "dotenv";

import path from "path";

// Configurate env vars
dotenv.config({ path: path.resolve(__dirname, "..", "..", "..", ".env") });

import { DATABASES_SUPPORT } from "./databasesSuported";

export const MONGO = {
  // configurando o database
  database: {
    dialect: process.env.DB_DIALECT || DATABASES_SUPPORT.MONGODB,
    url: process.env.DB_URI || "",
  },
};

export const SERVERLESS = {
  uri: process.env.SERVERLESS_URI,
};
