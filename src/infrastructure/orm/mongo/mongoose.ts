"use strict";

import * as mongoose from "mongoose";
import { MONGO } from "../../configs/envs";

mongoose.connect(MONGO.database.url);

const database = mongoose.connection;

// Verificar se ocorreu nenhum erro ao conectar
database.on("error", () => {
  console.log("Ops! error for conection in database!");
});

// Sucesso na conexão
database.once("open", () => {
  console.log("Yeaah! Conexão Successfully!");
});

export { database };
