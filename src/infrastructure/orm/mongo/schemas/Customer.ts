import { Schema, model, Model, connect, disconnect, Query, Document, Types } from "mongoose";

import * as envs from "./../../../configs/envs";

// doc: https://mongoosejs.com/docs/typescript/query-helpers.html

interface Customer {
  id: Types.ObjectId;
  name: string;
  details: string;
}

interface CustomertQueryHelpers {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  findAll(): Query<any, Document<Customer>> & CustomertQueryHelpers;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  update(): Query<any, Document<Customer>> & CustomertQueryHelpers;
}

// TODO: Conection mongoose shared for main file mongoose.ts.
// connection temp, for persist in dynamobd here for test no sql mongodb
const uri: string = envs.MONGO.database.url || "mongodb://user:pass@127.0.0.1:27018/admin";

const schema = new Schema<Customer, Model<Customer, CustomertQueryHelpers>>({
  name: { type: String, required: true },
  details: { type: String, required: true },
});

const CustomerModel = model<Customer>("Customer", schema);

// Todo: This functions needed refactor to implements in the other class with schema and connection
// this file share via Injection Dependecy Single responsability principle

export async function create(payload: Object): Promise<Object> {
  await connect(uri);

  const insertCustomer = new CustomerModel(payload);

  await insertCustomer.save();

  await disconnect();

  return payload;
}

export async function list(): Promise<Object> {
  await connect(uri);

  // return all data customers
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  schema.query.findAll = function (): Query<any, Document<Customer>> & CustomertQueryHelpers {
    return this.find({});
  };

  const response = await CustomerModel.find({});

  await disconnect();

  return response;
}

export async function update(id: string, name: string): Promise<Object> {
  await connect(uri);

  const filter = { _id: id };
  const update = { name };

  // running update for filter
  schema.query.findByIdAndUpdate = function (
    filter: Object,
    update: Object,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ): Query<any, Document<Customer>> & CustomertQueryHelpers {
    return this.findByIdAndUpdate(filter, update);
  };

  // Check update is running with success.
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  schema.query.findOne = function (filter: Object): Query<any, Document<Customer>> & CustomertQueryHelpers {
    return this.findOne(filter);
  };

  (await CustomerModel.findByIdAndUpdate(filter, update)) || {};
  const checkUpdate = (await CustomerModel.findOne(filter)) || {};

  await disconnect();

  return checkUpdate;
}

export async function deleteCustomer(payload: Object): Promise<Object> {
  await connect(uri);

  const { id } = payload as Customer;

  const filter = { _id: id };

  // running update for filter
  schema.query.delete = function (
    filter: Object,
    update: Object,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ): Query<any, Document<Customer>> & CustomertQueryHelpers {
    return this.delete(filter, update);
  };

  // Check update is running with success.
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  schema.query.delete = function (filter: Object): Query<any, Document<Customer>> & CustomertQueryHelpers {
    return this.delete(filter);
  };

  const deleteResult = (await CustomerModel.deleteOne(filter)) || {};

  await disconnect();

  return deleteResult;
}

export async function find(filter: Object): Promise<Object> {
  await connect(uri);

  // return all data customers
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  schema.query.findOne = function (): Query<any, Document<Customer>> & CustomertQueryHelpers {
    return this.findOne(filter);
  };

  const response = await CustomerModel.findOne(filter);

  await disconnect();

  return response as Customer;
}
