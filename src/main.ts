import { init, start } from "./server";

// Após resolução das configurações iniciar o servidor
init().then(() => start());
