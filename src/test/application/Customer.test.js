"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Customer_1 = require("../../src/domain/Customer");
const CustomerRepository_1 = require("../../src/domain/CustomerRepository");
jest.useFakeTimers();
// Configurate redis or database for mocks for integrated tests
describe("describe should pass all customer actions in API", () => {
    afterEach(() => {
        jest.useFakeTimers();
    });
    const mock = jest.fn(() => {
        return {
            id: "1997",
            name: "Wellington Bezerra",
        };
    });
    test("test create new Customer", () => __awaiter(void 0, void 0, void 0, function* () {
        jest.useFakeTimers();
        const mockCustomer = mock();
        const customer = new Customer_1.Customer(mockCustomer.name, mockCustomer.id);
        const repo = new CustomerRepository_1.CustomerRepository();
        repo.create = jest.fn(() => __awaiter(void 0, void 0, void 0, function* () {
            return {
                name: customer.name,
                id: customer.id,
            };
        }));
        const resultCallHandle = yield repo.create(customer.name, customer.id);
        expect(mockCustomer.name).toEqual(resultCallHandle.name);
    }));
    test("test read all Customer", () => __awaiter(void 0, void 0, void 0, function* () {
        jest.useFakeTimers();
        const mockCustomer = mock();
        const customer = new Customer_1.Customer(mockCustomer.name, mockCustomer.id);
        const repo = new CustomerRepository_1.CustomerRepository();
        repo.list = jest.fn(() => __awaiter(void 0, void 0, void 0, function* () {
            return {
                name: customer.name,
                id: customer.id,
            };
        }));
        const { name } = (yield repo.list());
        expect(mockCustomer.name).toEqual(name);
    }));
    test("test update one Customer", () => __awaiter(void 0, void 0, void 0, function* () {
        jest.useFakeTimers();
        const newName = "PagaLeve Fintech";
        const mockCustomer = mock();
        const customer = new Customer_1.Customer(newName, mockCustomer.id);
        const repo = new CustomerRepository_1.CustomerRepository();
        repo.update = jest.fn(() => __awaiter(void 0, void 0, void 0, function* () {
            return {
                name: newName,
                id: customer.id,
            };
        }));
        const resultCallHandle = (yield repo.update(customer.name, customer.id));
        expect(customer.name).toEqual(resultCallHandle.name);
    }));
    test("test delete one Customer", () => __awaiter(void 0, void 0, void 0, function* () {
        jest.useFakeTimers();
        const mockCustomer = mock();
        const customer = new Customer_1.Customer(mockCustomer.name, mockCustomer.id);
        const repo = new CustomerRepository_1.CustomerRepository();
        repo.delete = jest.fn(() => __awaiter(void 0, void 0, void 0, function* () {
            return {};
        }));
        const resultCallHandle = yield repo.delete(customer.name, customer.id);
        expect({}).toEqual(resultCallHandle);
    }));
    test("test search one Customer", () => __awaiter(void 0, void 0, void 0, function* () {
        jest.useFakeTimers();
        const mockCustomer = mock();
        const customer = new Customer_1.Customer(mockCustomer.name, mockCustomer.id);
        const repo = new CustomerRepository_1.CustomerRepository();
        repo.find = jest.fn(() => __awaiter(void 0, void 0, void 0, function* () {
            return {
                name: customer.name,
                id: customer.id,
            };
        }));
        const resultCallHandle = yield repo.find({
            name: customer.name,
            id: customer.id,
        });
        expect(customer).toEqual(resultCallHandle);
    }));
});
//# sourceMappingURL=Customer.test.js.map