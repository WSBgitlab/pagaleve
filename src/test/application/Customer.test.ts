import { Customer } from "../../domain/Customer";
import { CustomerRepository } from "../../domain/CustomerRepository";

jest.useFakeTimers();

interface ICustomer {
  name: string;
  details: string;
}

interface IListAllCustomers {
  data: Array<Customer>[];
}

// Configurate redis or database for mocks for integrated tests
describe("describe should pass all customer actions in API", () => {
  afterEach(() => {
    jest.useFakeTimers();
  });

  const mock = jest.fn<ICustomer, []>(() => {
    return {
      name: "Wellington Bezerra",
      details: "year 1997",
    };
  });

  test("test create new Customer", async () => {
    jest.useFakeTimers();

    const mockCustomer = mock();
    const customer = new Customer(mockCustomer.name, mockCustomer.details);
    const repo = new CustomerRepository();

    repo.create = jest.fn<Promise<Customer>, []>(async () => {
      return {
        id: customer.id,
        name: customer.name,
        details: customer.details,
      };
    });

    const resultCallHandle = (await repo.create(customer)) as Customer;

    expect(mockCustomer.name).toEqual(resultCallHandle.name);
  });

  test("test read all Customer", async () => {
    jest.useFakeTimers();

    const mockCustomer = mock();
    const customer = new Customer(mockCustomer.name, mockCustomer.details);
    const repo = new CustomerRepository();

    repo.list = jest.fn<Promise<IListAllCustomers>, []>(async () => {
      const fake = {
        id: customer.id,
        name: customer.name,
        details: customer.details,
      } as Customer;

      return {
        data: [],
      };
    });

    const data = (await repo.list()) as IListAllCustomers;

    expect(mockCustomer.name).toEqual(name);
  });

  test("test update one Customer", async () => {
    jest.useFakeTimers();
    const newName = "PagaLeve Fintech";

    const mockCustomer = mock();
    const customer = new Customer(newName, mockCustomer.details);
    const repo = new CustomerRepository();

    repo.update = jest.fn<Promise<Customer>, []>(async () => {
      return {
        id: "1234",
        name: newName,
        details: customer.details,
      } as Customer;
    });

    const resultCallHandle = (await repo.update(customer.name, customer.details)) as ICustomer;

    expect(customer.name).toEqual(resultCallHandle.name);
  });

  test("test delete one Customer", async () => {
    jest.useFakeTimers();

    const mockCustomer = mock();
    const customer = new Customer(mockCustomer.name, mockCustomer.details);
    const repo = new CustomerRepository();

    repo.delete = jest.fn<Promise<Customer>, []>(async () => {
      return {} as Customer;
    });

    const resultCallHandle = await repo.delete(customer.name, customer.details);

    expect({}).toEqual(resultCallHandle);
  });

  test("test search one Customer", async () => {
    jest.useFakeTimers();

    const mockCustomer = mock();
    const customer = new Customer(mockCustomer.name, mockCustomer.details);
    const repo = new CustomerRepository();

    repo.find = jest.fn<Promise<Customer>, []>(async () => {
      return {
        id: customer.id,
        name: customer.name,
        details: customer.details,
      };
    });

    const resultCallHandle = await repo.find({
      name: customer.name,
      details: customer.details,
    });

    expect(customer).toEqual(resultCallHandle);
  });
});
