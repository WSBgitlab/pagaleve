"use strict";

import * as jwt from "jsonwebtoken";

const mockUserAccept = "shhhh!";

// uuid
import { v4 as uuid } from "uuid";

// Login user.
export async function ValidAccessToken(headerToken: string) {
  try {
    // find in database.
    // I'm simulate data return the database

    // Here we can implement in repository the users find credentials is correct and validation
    // Todo: after the steps deploy, monitoring and serveless.

    // generate token for user.
    if (headerToken !== mockUserAccept) {
      throw new Error("Ops! Secret user invalid! Try again with user valid.");
    }

    // identify would be user id the database
    const token = jwt.sign({ identify: uuid().toString() }, mockUserAccept, {
      expiresIn: "2h",
    });

    return token;
  } catch (error) {
    throw Error("Error in verify token!");
  }
}
