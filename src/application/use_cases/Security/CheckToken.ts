"use strict";

// Simulate integration with database
const mockUserAccept = "shhhh!";

import * as jwt from "jsonwebtoken";

// uuid
import { v4 as uuid } from "uuid";

export async function checkToken(secret: string) {
  // find in database.
  // I'm simulate data return the database

  // Here we can implement in repository the users find credentials is correct and validation
  // Todo: after the steps deploy, monitoring and serveless.
  if (!secret || !secret.startsWith("Bearer ")) {
    throw new Error("Ops! Secret user invalid! Try again with user valid.");
  }

  // Regex for token get
  const mathToken = secret.replace(/Bearer/gi, "").replace(/ /g, "");

  // generate token for user.
  const checkToken = jwt.verify(mathToken, mockUserAccept);

  // return data the user or uuid the mongo or hash.
  return checkToken;
}
