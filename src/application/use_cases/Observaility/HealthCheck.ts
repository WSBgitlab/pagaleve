"use strict";

export async function healthCheck() {
  return { version: "1.0.0", status: "up" };
}
