import { CustomerRepository } from "../../../domain/CustomerRepository";
import { CustomerRepoMongo } from "../../../infrastructure/repositories/CustomerRepoMongo";
import { Customer } from "./../../../domain/Customer";

interface IListAllCustomers {
  data: Array<Customer>[];
}

export class ActionsCustomer implements CustomerRepository {
  async create(payload: Customer): Promise<Customer> {
    try {
      const repository = new CustomerRepoMongo();
      const { id, name, details } = (await repository.create(payload)) as Customer;

      return {
        id,
        name,
        details,
      };
    } catch (error) {
      throw new Error("Error create new Customer");
    }
  }

  async list(): Promise<IListAllCustomers> {
    try {
      const repository = new CustomerRepoMongo();

      const allData = await repository.list();

      return allData;
    } catch (error) {
      throw new Error("Error List all Customers");
    }
  }

  async find(filter: Customer): Promise<Customer> {
    try {
      const repository = new CustomerRepoMongo();

      const getCustomer = await repository.find(filter);

      return getCustomer;
    } catch (erro) {
      throw new Error("Error Find Customers" + erro);
    }
  }

  async update(name: string, id: string): Promise<Customer> {
    try {
      const repository = new CustomerRepoMongo();

      const updateData = await repository.update(id, name);

      return updateData;
    } catch (error) {
      throw new Error("Error the update Customer fields" + error);
    }
  }

  async delete(id: string): Promise<Customer> {
    try {
      const repository = new CustomerRepoMongo();

      const actionDelete = await repository.delete(id);

      return actionDelete;
    } catch (error) {
      throw new Error("Error to delete customer with id: " + id);
    }
  }

  async search(name: string, details: string): Promise<Customer> {
    try {
      const repository = new CustomerRepoMongo();

      const search = await repository.search(name, details);

      return search;
    } catch (error) {
      throw new Error(`Error to search with fields ${name} and ${details} customer`);
    }
  }
}
