"use strict";

import axios from "axios";
import * as envs from "../../../infrastructure/configs/envs";

const uriServerless = envs.SERVERLESS.uri;

export async function ServerCreateCustomer(body: Object) {
  // call in API Gateway
  const ServerlessResponse = await axios.post(`${uriServerless}/customer`, body);

  return ServerlessResponse.data;
}
