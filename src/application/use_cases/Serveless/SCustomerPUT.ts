"use strict";

import axios from "axios";
import * as envs from "../../../infrastructure/configs/envs";

const uriServerless = envs.SERVERLESS.uri;

export async function UpdatedCustomer(id: string, body: Object) {
  // call in API Gateway
  const ServerlessResponse = await axios.put(`${uriServerless}/customer/${id}`, body);

  return ServerlessResponse.data;
}
