"use strict";

import axios from "../../../infrastructure/Http/Axios";
import { Customer } from "../../../domain/Customer";
import { CustomerRepository } from "../../../domain/CustomerRepository";
import * as envs from "../../../infrastructure/configs/envs";

const uriServerless = envs.SERVERLESS.uri;

interface IListAllCustomers {
  data: Array<Customer>[];
}

export class ServerlessCustomer implements CustomerRepository {
  async create(payload: Customer): Promise<Customer> {
    const ServerlessResponse = await axios.post(`${uriServerless}/customer`, payload);

    return ServerlessResponse.data;
  }

  async find(filter: Customer): Promise<Customer> {
    // call in API Gateway
    const ServerlessResponse = await axios(`${uriServerless}/customer/${filter.id}`);

    return ServerlessResponse.data;
  }

  async list(): Promise<IListAllCustomers> {
    const ServerlessResponse = (await axios(`${uriServerless}/customer/list`)) as IListAllCustomers;

    return ServerlessResponse;
  }

  async update(name: string, id: string, details: string): Promise<Customer> {
    // call in API Gateway
    const ServerlessResponse = await axios.put(`${uriServerless}/customer/${id}`, {
      name,
      details,
    });

    return ServerlessResponse.data;
  }

  async delete(id: string): Promise<Customer> {
    // call in API Gateway
    const ServerlessResponse = await axios.delete(`${uriServerless}/customer/${id}`);

    return ServerlessResponse.data;
  }

  async search(name: string, details: string): Promise<Customer> {
    return;
  }
}
