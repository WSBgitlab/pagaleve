"use strict";

import axios from "axios";
import * as envs from "../../../infrastructure/configs/envs";

const uriServerless = envs.SERVERLESS.uri;

export async function ServerListCustomer() {
  // call in API Gateway
  const ServerlessResponse = await axios(`${uriServerless}/customer/list`);

  return ServerlessResponse.data;
}
