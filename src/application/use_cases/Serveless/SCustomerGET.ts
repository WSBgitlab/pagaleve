"use strict";

import axios from "axios";
import * as envs from "../../../infrastructure/configs/envs";

const uriServerless = envs.SERVERLESS.uri;

export async function ServerReadCustomer(id: string) {
  // call in API Gateway
  const ServerlessResponse = await axios(`${uriServerless}/customer/${id}`);

  return ServerlessResponse.data;
}
