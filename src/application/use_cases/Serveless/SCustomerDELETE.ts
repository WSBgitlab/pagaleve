"use strict";

import axios from "axios";
import * as envs from "../../../infrastructure/configs/envs";

const uriServerless = envs.SERVERLESS.uri;

export async function DeleteCustomer(id: string) {
  // call in API Gateway
  const ServerlessResponse = await axios.delete(`${uriServerless}/customer/${id}`);

  return ServerlessResponse.data;
}
