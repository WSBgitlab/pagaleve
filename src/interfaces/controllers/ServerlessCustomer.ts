import { ServerCreateCustomer } from "../../application/use_cases/Serveless/SCustomerPOST";
import { ServerReadCustomer } from "../../application/use_cases/Serveless/SCustomerGET";
import { ServerListCustomer } from "../../application/use_cases/Serveless/SCustomerList";
import { UpdatedCustomer } from "../../application/use_cases/Serveless/SCustomerPUT";
import { DeleteCustomer } from "../../application/use_cases/Serveless/SCustomerDELETE";

import { ServerlessCustomer } from "../../application/use_cases/Serveless/ServerCustomer";
import { Customer } from "../../domain/Customer";

interface IListAllCustomers {
  data: Array<Customer>[];
}

export class ControllerServerless extends ServerlessCustomer {
  async create(payload: Customer): Promise<Customer> {
    const actions = new ServerlessCustomer();

    const createResponse = await actions.create(payload);

    return createResponse;
  }

  async find(filter: Customer): Promise<Customer> {
    const actions = new ServerlessCustomer();
    const readResponse = await actions.find(filter);

    return readResponse;
  }

  async list(): Promise<IListAllCustomers> {
    const actions = new ServerlessCustomer();
    const listResponse = await actions.list();

    return listResponse;
  }

  async update(name: string, id: string, details: string): Promise<Customer> {
    const actions = new ServerlessCustomer();
    const updatedResponse = await actions.update(name, id, details);

    return updatedResponse;
  }

  async delete(id: string): Promise<Customer> {
    const actions = new ServerlessCustomer();
    const deletedResponse = await actions.delete(id);

    return deletedResponse;
  }
}

export async function ServerPostCustomer(body: Object) {
  const createResponse = await ServerCreateCustomer(body);

  return createResponse;
}

export async function ServerGetCustomer(id: string) {
  const readResponse = await ServerReadCustomer(id);

  return readResponse;
}

export async function ServerListCustomers() {
  const listResponse = await ServerListCustomer();

  return listResponse;
}

export async function ServerUpdateCustomer(id: string, body: Object) {
  const updatedResponse = await UpdatedCustomer(id, body);

  return updatedResponse;
}

export async function ServerDeletedCustomer(id: string) {
  const deletedResponse = await DeleteCustomer(id);

  return deletedResponse;
}
