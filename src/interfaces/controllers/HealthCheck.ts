"use strict";

import { healthCheck } from "./../../application/use_cases/Observaility/HealthCheck";

export function healthCheckController() {
  return healthCheck();
}
