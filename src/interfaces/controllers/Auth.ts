import { Request } from "@hapi/hapi";
import { checkToken } from "../../application/use_cases/Security/CheckToken";
import { ValidAccessToken } from "../../application/use_cases/Security/ValidAccessToken";

type SecretPayload = {
  secret: string;
};

// check if token is already in request
export async function checkTokenAccess(request: Request) {
  // data
  const { secret } = request.payload as SecretPayload;
  try {
    const generateToken = await checkToken(secret);

    return generateToken;
  } catch (err) {
    throw new Error("Secret bad request!");
  }
}

// validation if token is accept
export async function validateTokenAccess(request: Request) {
  const headerToken = request.headers.authorization;

  try {
    const secret = await ValidAccessToken(headerToken);

    return secret;
  } catch (error) {
    throw new Error("Credentials invalid!");
  }
}
