"use strict";

import { ActionsCustomer } from "../../application/use_cases/Customer/ActionsCustomer";
import { Customer } from "../../domain/Customer";

interface IListAllCustomers {
  data: Array<Customer>[];
}

export class ControllerCustomer extends ActionsCustomer {
  constructor() {
    super();
  }

  async create(payload: Customer): Promise<Customer> {
    const actions = new ActionsCustomer();
    const createResponse = await actions.create(payload);
    return createResponse;
  }

  async list(): Promise<IListAllCustomers> {
    const actions = new ActionsCustomer();
    const listResponse = await actions.list();

    return listResponse;
  }

  async find(filter: Customer): Promise<Customer> {
    const actions = new ActionsCustomer();
    const filterResponse = await actions.find(filter);
    return filterResponse;
  }

  async update(name: string, id: string): Promise<Customer> {
    const actions = new ActionsCustomer();
    const updateResponse = await actions.update(id, name);
    return updateResponse;
  }

  async delete(id: string): Promise<Customer> {
    const actions = new ActionsCustomer();
    const deleteResponse = await actions.delete(id);

    return deleteResponse;
  }

  async search(name: string, details: string): Promise<Customer> {
    const actions = new ActionsCustomer();
    const search = await actions.search(name, details);

    return search;
  }
}
