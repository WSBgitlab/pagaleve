"use strict";

import { Request, ResponseToolkit, ServerRoute } from "@hapi/hapi";
import { Joi } from "@docusaurus/utils-validation";

import { ControllerCustomer } from "./../controllers/Customer";
import { Customer } from "../../domain/Customer";

interface ICustomerDelete {
  id: string;
  name: string;
  details: string;
  deletedCount: number;
}

async function HandlePostCustomer(request: Request): Promise<Object> {
  // Todo: destructured how to do ts lint not  warnning erros sintaxe
  const customerActions = new ControllerCustomer();

  const data = request.payload as Customer;

  const response = await customerActions.create(data);

  return response;
}

async function HandleGetCustomer(): Promise<Object> {
  const customerActions = new ControllerCustomer();
  const response = await customerActions.list();

  return response;
}

async function HandlePutCustomer(request: Request, h: ResponseToolkit): Promise<Object> {
  const customerActions = new ControllerCustomer();
  const { id, name } = request.payload as Customer;
  const response = await customerActions.update(id, name);

  // Idempotent method
  if (Object.keys(response).length == 0) return h.response({ notify: "Not content for this data" }).code(204);

  return response;
}

async function HandleDeleteCustomer(request: Request, h: ResponseToolkit): Promise<Object> {
  const customerActions = new ControllerCustomer();
  const { id } = request.payload as Customer;
  const response = (await customerActions.delete(id)) as ICustomerDelete;

  // doc: https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Methods/DELETE
  if (response.deletedCount == 1)
    return {
      message: "Deleted customer with success",
      response,
    };

  return h.response({ notify: "Not content for this data" }).code(204);
}

async function HandleSearchCustomer(request: Request, h: ResponseToolkit): Promise<Object> {
  const customerActions = new ControllerCustomer();

  const { name, details } = request.query as Customer;

  const response = await customerActions.search(name, details);

  console.log(response, "response");
  // doc: https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Methods/DELETE
  if (response == null) {
    return h.response({ menssage: "Not customer with payload data" }).code(204);
  }

  return response;
}

const customerRoutes: ServerRoute[] = [
  {
    method: "POST",
    path: "/customer",
    handler: HandlePostCustomer,
    options: {
      description: "Create new Customer!",
      tags: ["api", "MongoCRUD"],
      validate: {
        payload: Joi.object({
          name: Joi.string().min(10).max(140),
          id: Joi.string().min(1).max(20),
        }),
        options: {
          allowUnknown: true,
        },
      },
    },
  },
  {
    method: "GET",
    path: "/customer",
    handler: HandleGetCustomer,
    options: {
      description: "List customer with name!",
      tags: ["api", "MongoCRUD"],
    },
  },
  {
    method: "GET",
    path: "/customer/search",
    handler: HandleSearchCustomer,
    options: {
      description: "Search customer with params!",
      tags: ["api", "MongoCRUD"],
    },
  },
  {
    method: "PUT",
    path: "/customer",
    handler: HandlePutCustomer,
    options: {
      description: "Update customer the find id and update name.",
      tags: ["api", "MongoCRUD"],
      validate: {
        payload: Joi.object({
          id: Joi.string().min(10).max(30),
          name: Joi.string().min(10).max(140),
          details: Joi.string().min(1).max(254),
        }),
        options: {
          allowUnknown: true,
        },
      },
    },
  },
  {
    method: "DELETE",
    path: "/customer",
    handler: HandleDeleteCustomer,
    options: {
      description: "Delete customer.",
      tags: ["api", "MongoCRUD"],
      validate: {
        payload: Joi.object({
          id: Joi.string().min(1).max(254),
        }),
        options: {
          allowUnknown: true,
        },
      },
    },
  },
];

export default customerRoutes;
