"use strict";

import { healthCheckController } from "./../controllers/HealthCheck";
import { ResponseToolkit, ServerRoute } from "@hapi/hapi";
import { checkToken } from "../../application/use_cases/Security/CheckToken";

async function HandleGetHealthCheck(): Promise<Object> {
  const response = await healthCheckController();

  return response;
}

const healthRoutes: ServerRoute[] = [
  {
    method: "GET",
    path: "/healthcheck",
    handler: HandleGetHealthCheck,
    options: {
      description: "Validation the app is running with health check app",
      tags: ["api", "monitoring"],
    },
  },
  {
    method: "GET",
    path: "/private",
    handler: async function (request) {
      const auth = request.headers.Authorization;
      await checkToken(auth);
      return HandleGetHealthCheck;
    },
    options: {
      description: "Validation the app is running with health check app",
      tags: ["api"],
    },
  },
];

export default healthRoutes;
