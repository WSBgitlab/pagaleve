"use strict";

import {
  ServerGetCustomer,
  ServerPostCustomer,
  ServerListCustomers,
  ServerUpdateCustomer,
  ServerDeletedCustomer,
  ControllerServerless,
} from "./../controllers/ServerlessCustomer";

import { Request, ServerRoute } from "@hapi/hapi";
import { Joi } from "@docusaurus/utils-validation";
import { Customer } from "../../domain/Customer";

async function HandleServerPostCustomer(request: Request): Promise<Object> {
  const actions = new ControllerServerless();
  const payload = request.payload as Customer;
  const response = await actions.create(payload);

  return response;
}

async function HandleServerGetCustomer(request: Request): Promise<Object> {
  const response = await ServerGetCustomer(request.params.id);

  return response;
}

async function HandleServerListCustomer(): Promise<Object> {
  const response = await ServerListCustomers();

  return response;
}

async function HandleServerUpdateCustomer(request: Request): Promise<Object> {
  const response = await ServerUpdateCustomer(request.params.id, request.payload);

  return response;
}

async function HandleServerDeleteCustomer(request: Request): Promise<Object> {
  const response = await ServerDeletedCustomer(request.params.id);

  return response;
}

const servercustomerRoutes: ServerRoute[] = [
  {
    method: "POST",
    path: "/server/",
    handler: HandleServerPostCustomer,
    options: {
      description: "Serverless: Create customer id data.",
      tags: ["api", "serveless Gateway API"],
      validate: {
        payload: Joi.object({
          name: Joi.string().min(10).max(140),
          details: Joi.string().min(10).max(140),
          when: Joi.string().min(10).max(140),
        }),
        options: {
          allowUnknown: true,
        },
      },
    },
  },
  {
    method: "GET",
    path: "/server/{id}",
    handler: HandleServerGetCustomer,
    options: {
      description: "Serverless: Read customer id data.",
      tags: ["api", "serveless Gateway API"],
    },
  },
  {
    method: "GET",
    path: "/server/list",
    handler: HandleServerListCustomer,
    options: {
      description: "Serverless: List customers.",
      tags: ["api", "serveless Gateway API"],
    },
  },
  {
    method: "PUT",
    path: "/server/{id}",
    handler: HandleServerUpdateCustomer,
    options: {
      description: "Serverless: Update fields customer id data.",
      tags: ["api", "serveless Gateway API"],
      validate: {
        payload: Joi.object({
          fields: Joi.string().min(1).max(254),
        }),
        options: {
          allowUnknown: true,
        },
      },
    },
  },
  {
    method: "DELETE",
    path: "/server/{id}",
    handler: HandleServerDeleteCustomer,
    options: {
      description: "Serverless: Delete customer with id data.",
      tags: ["api", "serveless Gateway API"],
    },
  },
];

export default servercustomerRoutes;
