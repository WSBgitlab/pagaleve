"use strict";

import { checkTokenAccess, validateTokenAccess } from "./../controllers/Auth";
import { Request, ServerRoute } from "@hapi/hapi";

async function HandleLoginToken(request: Request): Promise<Object> {
  try {
    const token = await validateTokenAccess(request);

    return { token };
  } catch (error) {
    throw new Error("erro" + error);
  }
}

async function HandleCheckToken(request: Request): Promise<Object> {
  try {
    const validToken = await checkTokenAccess(request);

    return {
      status: "Yeeeah! Token is valid user in database!",
      token: validToken,
    };
  } catch (error) {
    throw new Error("erro" + error);
  }
}

const securityRoutes: ServerRoute[] = [
  {
    method: "POST",
    path: "/login",
    handler: HandleLoginToken,
    options: {
      description: "Login app with mock secret users!",
      tags: ["api", "security"],
    },
  },
  {
    method: "POST",
    path: "/token",
    handler: HandleCheckToken,
    options: {
      description: "Validation token generated for /login",
      tags: ["api", "security"],
    },
  },
];

export default securityRoutes;
