# PagaLeve - Challenger 🤟

## Resumo

- [Overview](#overview)
- [Configuração](#configuração-inicial)
- [Especificação Técninca](#especificação-técnica)
- [Considerações Finais](#Considerações)

# Desafio Backend -

![Badge](https://img.shields.io/static/v1?label=License&message=MIT&color=green&style=plastic)
![Badge](https://img.shields.io/static/v1?label=Node&message=14&color=yellow&style=plastic)
![Badge](https://img.shields.io/static/v1?label=Mongodb&message=LTS&color=greenw&style=plastic)

## Overview

Realização de teste técnico para avaliação do time paga leve para novo emprego.

### Pré requisitos

Antes de inciar, você precisará de ter as seguintes ferramentas:
[Git](https://git-scm.com), [Node.js](https://nodejs.org/en/)

### Configuração inicial

<p>:computer:</p>

```bash
# Clone este repositório
$ git clone https://gitlab.com/WSBgitlab/pagaleve.git

# Acesse a pasta do projeto no terminal/cmd
$ cd

# Instale as dependências
$ npm install || yarn

# Antes de executar devemos configurar as variáveis de ambiente
# criaremos os arquivos de controle configure conforme seu ambiente
touch .env
touch .env.staging
touch .env.production

# Execute o compile do typescript
$ yarn dev:tsc

# Nota: Após realizar o cancelamento com o comando (ctrl + C) para matar o serviço.
# (temp) - Algumas vezes é necessário matar o processo com o kill no linux.
# (temp) - Ajustes serão realizado para que isso não aconteça.

# Subindo o servidor local
$ yarn dev:serve
```

App em execução.

![APP RUNNING](./doc/assets/running.png)

_template .env.example_

```bash

# Nota: Estou colocando as credênciais para nível de avaliação, jamais será fornecido essas envs públicamente.

DB_URI=mongodb://admin:pass_security@127.0.0.1:27018/admin
SERVERLESS_URI=https://rstmmsctt2.execute-api.us-west-2.amazonaws.com
```

_Swagger Documentation_

![SWAGGER DOCUMENTATION](./doc/assets/swagger.png)

### Especificação Técnica

Apresentação com resumos sobre a arquitetura do aplicativo/API.

#### Docker compose

Utilização de docker compose para nossa imagem do MongoDB, realizei crud também de um banco fora da AWS.
Comando para executar a imagem e conseguir utilizar a API de forma hosting local ou em um servidor em _staging_ e _production_

```bash
$ docker-compose up . || docker-compose up . -d # -d background action

#Verificando status da imagem
$ docker ps

#CONTAINER ID   IMAGE      COMMAND                  CREATED        STATUS       PORTS                                           NAMES
#b7f177105729   mongo      "docker-entrypoint.s…"   2 days ago     Up 4 hours   0.0.0.0:27018->27017/tcp, :::27018->27017/tcp   pagaleve_mongo_1
```

Utilização do banco como admin apenas para avaliação técnica.

_Nota: Como foco é uma aplicação Serveless, deixei a collection no database default como demonstrado na string de conexão no tópico de vars envs para outros ambientes seria configurado mediante aos requisitos do ambiente_

#### Requisições HTTP com Insominia

Preparado um arquivo da ferramenta insomnia para automação e collections de requisições para testes e facilitação no ambiente de dev
o arquivo se encontra em _doc/insomnia/pagaleve_

#### API endpoint security

Desenvolvimento de rotas seguras, implementei uma abordagem de rota segura com simulação de usuário credenciados junto aos endpoints:

- /login - Simulando um usuário para se logar, neste caso há um fake com valor de 'shhhhh!' para simular um retorno de cadastro de usuários.
- /token - Para validação do token gerado no endpoint anterior com JWT (JSON Web Tokens)
- /private - Handle endpoint com usuário credenciado somente.

Referências:

- [JWT JSON Web Tokens](https://jwt.io/)

#### Clean Architecture

Realizei o projeto com conceitos do arquitetura limpa para demonstrar meu conhecimento de padrões com essa arquitetura. Estou buscando conhecimentos e aprimorando no dia a dia, processos e conceitos relacionados a clean code e clean arc. Responsabilidades por camadas e atribuição de funções e classes para implementação.

Arquitetura de pastas 📁

```bash
├── src
│   ├── application
│   │   ├── security
│   │   │   ├── Token.ts
            # Casos de uso - Implementação de funcionalidades do app/api;
│   │   ├── use_cases
│   │   │   ├── CreateCustomer.ts
│   │   │   ├── ReadCustomer.ts
│   │   │   ├── UpdateCustomer.ts
│   │   │   ├── DeleteCustomer.ts
│   │   │   ├── HealthCheck.ts
            # Dominio - Implementação camada dominio responsável para separar responsabilidades entre as features;
│   ├── domain
│   │   ├── Customer.ts
│   │   ├── CustomerRepository.ts
            # Infra - Resposavel por configurações de mecânismos externos e auxiliar a configuração do app/API ;
│   ├── infrastructure
│   │   ├── configs
│   │   │   ├── databasesSuported.ts
│   │   │   ├── envs.ts
│   │   ├── orm
│   │   │   ├── mongo
│   │   │   |   ├── schemas
│   │   │   |   |   ├── Customer.ts
│   │   │   |   ├── mongoose.ts
│   │   ├── repositories
            # Interfaces ou Adapters - Responsável pela conversão de dados para melhor utilização;
│   ├── interfaces
│   │   ├── controllers
│   │   │   ├── Customer.ts
│   │   │   ├── HealthCheck.ts
│   │   ├── routes
│   │   │   ├── Customer.ts
│   │   │   ├── HealthCheck.ts
        # main e server - arquivos com definições, configuração e iniciação do servidor;
│   ├── main.ts
│   ├── server.ts
        # Test - Todos os testes de unidade e integração separadas por camadas da aplicação
├── test
│   ├── application
│   │   ├── CreateCustomer.test.ts
├── lib
├── readme.md
├── package.json
├── .eslintrc.json
├── docker-compose.yml
├── jest.config.js
└── .gitignore
```

### Deploy Heroku

Foi criado, dois projetos no Heroku para ambientes de staging e production. O processo de deploy acontece a cada commit para o branch de staging e produção _inicialmente comecei com o de staging_, depois esse processo irei alterar para manual assim é a melhor prática para subir novas versões do app, após discussões e acordos entre o time.

![HEROKU APPS](./doc/assets/heroku.png)

_(temp): CI/CD para ambiente de staging não está conseguindo subir o processo do app - ajustando_

### Tecnologias 🛠

As seguintes ferramentas foram usadas na construção do projeto:

- [Node.js](https://nodejs.org/en/)
- [Typescript](https://www.typescriptlang.org/)
- [NPM](https://www.npmjs.com/get-npm)
- [GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

#### Considerações Finais

Queria agradecer à todas as pessoas que estão envolvidas nesse processo, ficarei muito feliz em embarcar na pagaleve, ajudar a empresa para alavancar e conquistar metas. Além de tudo trabalhar com pessoas que com pouco contato já sentir a energia da empresa, espero que consiga passar na avaliação e iniciar o novo job.

Agradecer todos aos envolvidos.

Obrigado!
