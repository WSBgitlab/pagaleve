"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.start = exports.init = exports.server = void 0;
const hapi_1 = __importDefault(require("@hapi/hapi"));
// import routes
const Index_1 = require("./interfaces/routes/Index");
const HapiSwagger = __importStar(require("hapi-swagger"));
const Inert = __importStar(require("@hapi/inert"));
const Vision = __importStar(require("@hapi/vision"));
// Configuração Hapi
const init = function () {
    return __awaiter(this, void 0, void 0, function* () {
        exports.server = hapi_1.default.server({
            port: process.env.PORT || 3000,
            host: process.env.SERVER || "localhost",
        });
        const swaggerOptions = {
            info: {
                title: "Pagaleve - Challenger API Documentation",
            },
        };
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const plugins = [
            {
                plugin: Inert,
            },
            {
                plugin: Vision,
            },
            {
                plugin: HapiSwagger,
                options: swaggerOptions,
            },
        ];
        yield exports.server.register(plugins);
        // Todo: refactor to index export file.
        exports.server.route(Index_1.healthRoutes);
        exports.server.route(Index_1.customerRoutes);
        exports.server.route(Index_1.securityRoutes);
        exports.server.route(Index_1.servercustomerRoutes);
        return exports.server;
    });
};
exports.init = init;
// Iniciando web server com HAPI
const start = function () {
    return __awaiter(this, void 0, void 0, function* () {
        console.log(`Server running: ${exports.server.settings.host}- port: ${exports.server.settings.port}`);
        return exports.server.start();
    });
};
exports.start = start;
process.on("unhandledRejection", (err) => {
    console.error("unhandledRejection");
    console.error(err);
    process.exit(1);
});
//# sourceMappingURL=Server.js.map