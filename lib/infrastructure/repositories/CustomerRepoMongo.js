"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerRepoMongo = void 0;
const CustomerRepository_1 = require("./../../domain/CustomerRepository");
const Customer_1 = require("./../orm/mongo/schemas/Customer");
class CustomerRepoMongo extends CustomerRepository_1.CustomerRepository {
    constructor() {
        super();
    }
    // Todo: remove any Type for accept ICustomer and response Object
    create(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const newCustomer = (yield (0, Customer_1.create)(payload));
                return newCustomer;
            }
            catch (error) {
                throw new Error("Not success in create new Customer!");
            }
        });
    }
    find(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const findCustomer = yield (0, Customer_1.find)(filter);
                return findCustomer;
            }
            catch (error) {
                throw new Error("Not success in create new Customer!");
            }
        });
    }
    list() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const ListAllCustomer = (yield (0, Customer_1.list)());
                return ListAllCustomer;
            }
            catch (error) {
                throw new Error("Not success in create new Customer!");
            }
        });
    }
    update(id, name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const updateCustomer = (yield (0, Customer_1.update)(id, name));
                return updateCustomer;
            }
            catch (error) {
                throw new Error("Not success in create new Customer!");
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const deleteC = yield (0, Customer_1.deleteCustomer)({ id });
                return deleteC;
            }
            catch (error) {
                throw new Error("Not success in create new Customer!");
            }
        });
    }
    search(name, details) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const searchCustomer = yield (0, Customer_1.find)({ name, details });
                console.log(searchCustomer, "search");
                return searchCustomer;
            }
            catch (error) {
                throw new Error("Not success in create new Customer!");
            }
        });
    }
}
exports.CustomerRepoMongo = CustomerRepoMongo;
//# sourceMappingURL=CustomerRepoMongo.js.map