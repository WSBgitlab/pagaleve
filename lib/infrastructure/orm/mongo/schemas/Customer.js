"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.find = exports.deleteCustomer = exports.update = exports.list = exports.create = void 0;
const mongoose_1 = require("mongoose");
const envs = __importStar(require("./../../../configs/envs"));
// TODO: Conection mongoose shared for main file mongoose.ts.
// connection temp, for persist in dynamobd here for test no sql mongodb
const uri = envs.MONGO.database.url || "mongodb://user:pass@127.0.0.1:27018/admin";
const schema = new mongoose_1.Schema({
    name: { type: String, required: true },
    details: { type: String, required: true },
});
const CustomerModel = (0, mongoose_1.model)("Customer", schema);
// Todo: This functions needed refactor to implements in the other class with schema and connection
// this file share via Injection Dependecy Single responsability principle
function create(payload) {
    return __awaiter(this, void 0, void 0, function* () {
        yield (0, mongoose_1.connect)(uri);
        const insertCustomer = new CustomerModel(payload);
        yield insertCustomer.save();
        yield (0, mongoose_1.disconnect)();
        return payload;
    });
}
exports.create = create;
function list() {
    return __awaiter(this, void 0, void 0, function* () {
        yield (0, mongoose_1.connect)(uri);
        // return all data customers
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        schema.query.findAll = function () {
            return this.find({});
        };
        const response = yield CustomerModel.find({});
        yield (0, mongoose_1.disconnect)();
        return response;
    });
}
exports.list = list;
function update(id, name) {
    return __awaiter(this, void 0, void 0, function* () {
        yield (0, mongoose_1.connect)(uri);
        const filter = { _id: id };
        const update = { name };
        // running update for filter
        schema.query.findByIdAndUpdate = function (filter, update) {
            return this.findByIdAndUpdate(filter, update);
        };
        // Check update is running with success.
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        schema.query.findOne = function (filter) {
            return this.findOne(filter);
        };
        (yield CustomerModel.findByIdAndUpdate(filter, update)) || {};
        const checkUpdate = (yield CustomerModel.findOne(filter)) || {};
        yield (0, mongoose_1.disconnect)();
        return checkUpdate;
    });
}
exports.update = update;
function deleteCustomer(payload) {
    return __awaiter(this, void 0, void 0, function* () {
        yield (0, mongoose_1.connect)(uri);
        const { id } = payload;
        const filter = { _id: id };
        // running update for filter
        schema.query.delete = function (filter, update) {
            return this.delete(filter, update);
        };
        // Check update is running with success.
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        schema.query.delete = function (filter) {
            return this.delete(filter);
        };
        const deleteResult = (yield CustomerModel.deleteOne(filter)) || {};
        yield (0, mongoose_1.disconnect)();
        return deleteResult;
    });
}
exports.deleteCustomer = deleteCustomer;
function find(filter) {
    return __awaiter(this, void 0, void 0, function* () {
        yield (0, mongoose_1.connect)(uri);
        // return all data customers
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        schema.query.findOne = function () {
            return this.findOne(filter);
        };
        const response = yield CustomerModel.findOne(filter);
        yield (0, mongoose_1.disconnect)();
        return response;
    });
}
exports.find = find;
//# sourceMappingURL=Customer.js.map