"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerlessCustomer = void 0;
const Axios_1 = __importDefault(require("../../../infrastructure/Http/Axios"));
const envs = __importStar(require("../../../infrastructure/configs/envs"));
const uriServerless = envs.SERVERLESS.uri;
class ServerlessCustomer {
    create(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            const ServerlessResponse = yield Axios_1.default.post(`${uriServerless}/customer`, payload);
            return ServerlessResponse.data;
        });
    }
    find(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            // call in API Gateway
            const ServerlessResponse = yield (0, Axios_1.default)(`${uriServerless}/customer/${filter.id}`);
            return ServerlessResponse.data;
        });
    }
    list() {
        return __awaiter(this, void 0, void 0, function* () {
            const ServerlessResponse = (yield (0, Axios_1.default)(`${uriServerless}/customer/list`));
            return ServerlessResponse;
        });
    }
    update(name, id, details) {
        return __awaiter(this, void 0, void 0, function* () {
            // call in API Gateway
            const ServerlessResponse = yield Axios_1.default.put(`${uriServerless}/customer/${id}`, {
                name,
                details,
            });
            return ServerlessResponse.data;
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            // call in API Gateway
            const ServerlessResponse = yield Axios_1.default.delete(`${uriServerless}/customer/${id}`);
            return ServerlessResponse.data;
        });
    }
}
exports.ServerlessCustomer = ServerlessCustomer;
//# sourceMappingURL=ServerCustomer.js.map