"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActionsCustomer = void 0;
const CustomerRepoMongo_1 = require("../../../infrastructure/repositories/CustomerRepoMongo");
class ActionsCustomer {
    create(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const repository = new CustomerRepoMongo_1.CustomerRepoMongo();
                const { id, name, details } = (yield repository.create(payload));
                return {
                    id,
                    name,
                    details,
                };
            }
            catch (error) {
                throw new Error("Error create new Customer");
            }
        });
    }
    list() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const repository = new CustomerRepoMongo_1.CustomerRepoMongo();
                const allData = yield repository.list();
                return allData;
            }
            catch (error) {
                throw new Error("Error List all Customers");
            }
        });
    }
    find(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const repository = new CustomerRepoMongo_1.CustomerRepoMongo();
                const getCustomer = yield repository.find(filter);
                return getCustomer;
            }
            catch (erro) {
                throw new Error("Error Find Customers" + erro);
            }
        });
    }
    update(name, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const repository = new CustomerRepoMongo_1.CustomerRepoMongo();
                const updateData = yield repository.update(id, name);
                return updateData;
            }
            catch (error) {
                throw new Error("Error the update Customer fields" + error);
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const repository = new CustomerRepoMongo_1.CustomerRepoMongo();
                const actionDelete = yield repository.delete(id);
                return actionDelete;
            }
            catch (error) {
                throw new Error("Error to delete customer with id: " + id);
            }
        });
    }
    search(name, details) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const repository = new CustomerRepoMongo_1.CustomerRepoMongo();
                const search = yield repository.search(name, details);
                return search;
            }
            catch (error) {
                throw new Error(`Error to search with fields ${name} and ${details} customer`);
            }
        });
    }
}
exports.ActionsCustomer = ActionsCustomer;
//# sourceMappingURL=ActionsCustomer.js.map