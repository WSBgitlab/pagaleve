"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkToken = void 0;
// Simulate integration with database
const mockUserAccept = "shhhh!";
const jwt = __importStar(require("jsonwebtoken"));
function checkToken(secret) {
    return __awaiter(this, void 0, void 0, function* () {
        // find in database.
        // I'm simulate data return the database
        // Here we can implement in repository the users find credentials is correct and validation
        // Todo: after the steps deploy, monitoring and serveless.
        if (!secret || !secret.startsWith("Bearer ")) {
            throw new Error("Ops! Secret user invalid! Try again with user valid.");
        }
        // Regex for token get
        const mathToken = secret.replace(/Bearer/gi, "").replace(/ /g, "");
        // generate token for user.
        const checkToken = jwt.verify(mathToken, mockUserAccept);
        // return data the user or uuid the mongo or hash.
        return checkToken;
    });
}
exports.checkToken = checkToken;
//# sourceMappingURL=CheckToken.js.map