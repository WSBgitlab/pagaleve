"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.servercustomerRoutes = exports.securityRoutes = exports.healthRoutes = exports.customerRoutes = void 0;
var Customer_1 = require("./Customer");
Object.defineProperty(exports, "customerRoutes", { enumerable: true, get: function () { return __importDefault(Customer_1).default; } });
var Helthcheck_1 = require("./Helthcheck");
Object.defineProperty(exports, "healthRoutes", { enumerable: true, get: function () { return __importDefault(Helthcheck_1).default; } });
var Security_1 = require("./Security");
Object.defineProperty(exports, "securityRoutes", { enumerable: true, get: function () { return __importDefault(Security_1).default; } });
var ServerCustomer_1 = require("./ServerCustomer");
Object.defineProperty(exports, "servercustomerRoutes", { enumerable: true, get: function () { return __importDefault(ServerCustomer_1).default; } });
//# sourceMappingURL=Index.js.map