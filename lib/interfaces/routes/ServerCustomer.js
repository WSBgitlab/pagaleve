"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ServerlessCustomer_1 = require("./../controllers/ServerlessCustomer");
const utils_validation_1 = require("@docusaurus/utils-validation");
function HandleServerPostCustomer(request) {
    return __awaiter(this, void 0, void 0, function* () {
        const actions = new ServerlessCustomer_1.ControllerServerless();
        const payload = request.payload;
        const response = yield actions.create(payload);
        return response;
    });
}
function HandleServerGetCustomer(request) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield (0, ServerlessCustomer_1.ServerGetCustomer)(request.params.id);
        return response;
    });
}
function HandleServerListCustomer() {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield (0, ServerlessCustomer_1.ServerListCustomers)();
        return response;
    });
}
function HandleServerUpdateCustomer(request) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield (0, ServerlessCustomer_1.ServerUpdateCustomer)(request.params.id, request.payload);
        return response;
    });
}
function HandleServerDeleteCustomer(request) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield (0, ServerlessCustomer_1.ServerDeletedCustomer)(request.params.id);
        return response;
    });
}
const servercustomerRoutes = [
    {
        method: "POST",
        path: "/server/",
        handler: HandleServerPostCustomer,
        options: {
            description: "Serverless: Create customer id data.",
            tags: ["api", "serveless Gateway API"],
            validate: {
                payload: utils_validation_1.Joi.object({
                    name: utils_validation_1.Joi.string().min(10).max(140),
                    details: utils_validation_1.Joi.string().min(10).max(140),
                    when: utils_validation_1.Joi.string().min(10).max(140),
                }),
                options: {
                    allowUnknown: true,
                },
            },
        },
    },
    {
        method: "GET",
        path: "/server/{id}",
        handler: HandleServerGetCustomer,
        options: {
            description: "Serverless: Read customer id data.",
            tags: ["api", "serveless Gateway API"],
        },
    },
    {
        method: "GET",
        path: "/server/list",
        handler: HandleServerListCustomer,
        options: {
            description: "Serverless: List customers.",
            tags: ["api", "serveless Gateway API"],
        },
    },
    {
        method: "PUT",
        path: "/server/{id}",
        handler: HandleServerUpdateCustomer,
        options: {
            description: "Serverless: Update fields customer id data.",
            tags: ["api", "serveless Gateway API"],
            validate: {
                payload: utils_validation_1.Joi.object({
                    fields: utils_validation_1.Joi.string().min(1).max(254),
                }),
                options: {
                    allowUnknown: true,
                },
            },
        },
    },
    {
        method: "DELETE",
        path: "/server/{id}",
        handler: HandleServerDeleteCustomer,
        options: {
            description: "Serverless: Delete customer with id data.",
            tags: ["api", "serveless Gateway API"],
        },
    },
];
exports.default = servercustomerRoutes;
//# sourceMappingURL=ServerCustomer.js.map