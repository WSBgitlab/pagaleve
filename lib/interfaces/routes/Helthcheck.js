"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const HealthCheck_1 = require("./../controllers/HealthCheck");
const CheckToken_1 = require("../../application/use_cases/Security/CheckToken");
function HandleGetHealthCheck() {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield (0, HealthCheck_1.healthCheckController)();
        return response;
    });
}
const healthRoutes = [
    {
        method: "GET",
        path: "/healthcheck",
        handler: HandleGetHealthCheck,
        options: {
            description: "Validation the app is running with health check app",
            tags: ["api", "monitoring"],
        },
    },
    {
        method: "GET",
        path: "/private",
        handler: function (request) {
            return __awaiter(this, void 0, void 0, function* () {
                const auth = request.headers.Authorization;
                yield (0, CheckToken_1.checkToken)(auth);
                return HandleGetHealthCheck;
            });
        },
        options: {
            description: "Validation the app is running with health check app",
            tags: ["api"],
        },
    },
];
exports.default = healthRoutes;
//# sourceMappingURL=Helthcheck.js.map