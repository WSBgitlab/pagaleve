"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Auth_1 = require("./../controllers/Auth");
function HandleLoginToken(request) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const token = yield (0, Auth_1.validateTokenAccess)(request);
            return { token };
        }
        catch (error) {
            throw new Error("erro" + error);
        }
    });
}
function HandleCheckToken(request) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const validToken = yield (0, Auth_1.checkTokenAccess)(request);
            return {
                status: "Yeeeah! Token is valid user in database!",
                token: validToken,
            };
        }
        catch (error) {
            throw new Error("erro" + error);
        }
    });
}
const securityRoutes = [
    {
        method: "POST",
        path: "/login",
        handler: HandleLoginToken,
        options: {
            description: "Login app with mock secret users!",
            tags: ["api", "security"],
        },
    },
    {
        method: "POST",
        path: "/token",
        handler: HandleCheckToken,
        options: {
            description: "Validation token generated for /login",
            tags: ["api", "security"],
        },
    },
];
exports.default = securityRoutes;
//# sourceMappingURL=Security.js.map