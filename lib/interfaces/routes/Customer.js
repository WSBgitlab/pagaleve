"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const utils_validation_1 = require("@docusaurus/utils-validation");
const Customer_1 = require("./../controllers/Customer");
function HandlePostCustomer(request) {
    return __awaiter(this, void 0, void 0, function* () {
        // Todo: destructured how to do ts lint not  warnning erros sintaxe
        const customerActions = new Customer_1.ControllerCustomer();
        const data = request.payload;
        const response = yield customerActions.create(data);
        return response;
    });
}
function HandleGetCustomer() {
    return __awaiter(this, void 0, void 0, function* () {
        const customerActions = new Customer_1.ControllerCustomer();
        const response = yield customerActions.list();
        return response;
    });
}
function HandlePutCustomer(request, h) {
    return __awaiter(this, void 0, void 0, function* () {
        const customerActions = new Customer_1.ControllerCustomer();
        const { id, name } = request.payload;
        const response = yield customerActions.update(id, name);
        // Idempotent method
        if (Object.keys(response).length == 0)
            return h.response({ notify: "Not content for this data" }).code(204);
        return response;
    });
}
function HandleDeleteCustomer(request, h) {
    return __awaiter(this, void 0, void 0, function* () {
        const customerActions = new Customer_1.ControllerCustomer();
        const { id } = request.payload;
        const response = (yield customerActions.delete(id));
        // doc: https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Methods/DELETE
        if (response.deletedCount == 1)
            return {
                message: "Deleted customer with success",
                response,
            };
        return h.response({ notify: "Not content for this data" }).code(204);
    });
}
function HandleSearchCustomer(request, h) {
    return __awaiter(this, void 0, void 0, function* () {
        const customerActions = new Customer_1.ControllerCustomer();
        const { name, details } = request.query;
        const response = yield customerActions.search(name, details);
        console.log(response, "response");
        // doc: https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Methods/DELETE
        if (response == null) {
            return h.response({ menssage: "Not customer with payload data" }).code(204);
        }
        return response;
    });
}
const customerRoutes = [
    {
        method: "POST",
        path: "/customer",
        handler: HandlePostCustomer,
        options: {
            description: "Create new Customer!",
            tags: ["api", "MongoCRUD"],
            validate: {
                payload: utils_validation_1.Joi.object({
                    name: utils_validation_1.Joi.string().min(10).max(140),
                    id: utils_validation_1.Joi.string().min(1).max(20),
                }),
                options: {
                    allowUnknown: true,
                },
            },
        },
    },
    {
        method: "GET",
        path: "/customer",
        handler: HandleGetCustomer,
        options: {
            description: "List customer with name!",
            tags: ["api", "MongoCRUD"],
        },
    },
    {
        method: "GET",
        path: "/customer/search",
        handler: HandleSearchCustomer,
        options: {
            description: "Search customer with params!",
            tags: ["api", "MongoCRUD"],
        },
    },
    {
        method: "PUT",
        path: "/customer",
        handler: HandlePutCustomer,
        options: {
            description: "Update customer the find id and update name.",
            tags: ["api", "MongoCRUD"],
            validate: {
                payload: utils_validation_1.Joi.object({
                    id: utils_validation_1.Joi.string().min(10).max(30),
                    name: utils_validation_1.Joi.string().min(10).max(140),
                    details: utils_validation_1.Joi.string().min(1).max(254),
                }),
                options: {
                    allowUnknown: true,
                },
            },
        },
    },
    {
        method: "DELETE",
        path: "/customer",
        handler: HandleDeleteCustomer,
        options: {
            description: "Delete customer.",
            tags: ["api", "MongoCRUD"],
            validate: {
                payload: utils_validation_1.Joi.object({
                    id: utils_validation_1.Joi.string().min(1).max(254),
                }),
                options: {
                    allowUnknown: true,
                },
            },
        },
    },
];
exports.default = customerRoutes;
//# sourceMappingURL=Customer.js.map