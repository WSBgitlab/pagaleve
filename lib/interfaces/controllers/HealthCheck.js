"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.healthCheckController = void 0;
const HealthCheck_1 = require("./../../application/use_cases/Observaility/HealthCheck");
function healthCheckController() {
    return (0, HealthCheck_1.healthCheck)();
}
exports.healthCheckController = healthCheckController;
//# sourceMappingURL=HealthCheck.js.map