"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerDeletedCustomer = exports.ServerUpdateCustomer = exports.ServerListCustomers = exports.ServerGetCustomer = exports.ServerPostCustomer = exports.ControllerServerless = void 0;
const SCustomerPOST_1 = require("../../application/use_cases/Serveless/SCustomerPOST");
const SCustomerGET_1 = require("../../application/use_cases/Serveless/SCustomerGET");
const SCustomerList_1 = require("../../application/use_cases/Serveless/SCustomerList");
const SCustomerPUT_1 = require("../../application/use_cases/Serveless/SCustomerPUT");
const SCustomerDELETE_1 = require("../../application/use_cases/Serveless/SCustomerDELETE");
const ServerCustomer_1 = require("../../application/use_cases/Serveless/ServerCustomer");
class ControllerServerless extends ServerCustomer_1.ServerlessCustomer {
    create(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            const actions = new ServerCustomer_1.ServerlessCustomer();
            const createResponse = yield actions.create(payload);
            return createResponse;
        });
    }
    find(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const actions = new ServerCustomer_1.ServerlessCustomer();
            const readResponse = yield actions.find(filter);
            return readResponse;
        });
    }
    list() {
        return __awaiter(this, void 0, void 0, function* () {
            const actions = new ServerCustomer_1.ServerlessCustomer();
            const listResponse = yield actions.list();
            return listResponse;
        });
    }
    update(name, id, details) {
        return __awaiter(this, void 0, void 0, function* () {
            const actions = new ServerCustomer_1.ServerlessCustomer();
            const updatedResponse = yield actions.update(name, id, details);
            return updatedResponse;
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const actions = new ServerCustomer_1.ServerlessCustomer();
            const deletedResponse = yield actions.delete(id);
            return deletedResponse;
        });
    }
}
exports.ControllerServerless = ControllerServerless;
function ServerPostCustomer(body) {
    return __awaiter(this, void 0, void 0, function* () {
        const createResponse = yield (0, SCustomerPOST_1.ServerCreateCustomer)(body);
        return createResponse;
    });
}
exports.ServerPostCustomer = ServerPostCustomer;
function ServerGetCustomer(id) {
    return __awaiter(this, void 0, void 0, function* () {
        const readResponse = yield (0, SCustomerGET_1.ServerReadCustomer)(id);
        return readResponse;
    });
}
exports.ServerGetCustomer = ServerGetCustomer;
function ServerListCustomers() {
    return __awaiter(this, void 0, void 0, function* () {
        const listResponse = yield (0, SCustomerList_1.ServerListCustomer)();
        return listResponse;
    });
}
exports.ServerListCustomers = ServerListCustomers;
function ServerUpdateCustomer(id, body) {
    return __awaiter(this, void 0, void 0, function* () {
        const updatedResponse = yield (0, SCustomerPUT_1.UpdatedCustomer)(id, body);
        return updatedResponse;
    });
}
exports.ServerUpdateCustomer = ServerUpdateCustomer;
function ServerDeletedCustomer(id) {
    return __awaiter(this, void 0, void 0, function* () {
        const deletedResponse = yield (0, SCustomerDELETE_1.DeleteCustomer)(id);
        return deletedResponse;
    });
}
exports.ServerDeletedCustomer = ServerDeletedCustomer;
//# sourceMappingURL=ServerlessCustomer.js.map