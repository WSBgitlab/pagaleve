"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateTokenAccess = exports.checkTokenAccess = void 0;
const CheckToken_1 = require("../../application/use_cases/Security/CheckToken");
const ValidAccessToken_1 = require("../../application/use_cases/Security/ValidAccessToken");
// check if token is already in request
function checkTokenAccess(request) {
    return __awaiter(this, void 0, void 0, function* () {
        // data
        const { secret } = request.payload;
        try {
            const generateToken = yield (0, CheckToken_1.checkToken)(secret);
            return generateToken;
        }
        catch (err) {
            throw new Error("Secret bad request!");
        }
    });
}
exports.checkTokenAccess = checkTokenAccess;
// validation if token is accept
function validateTokenAccess(request) {
    return __awaiter(this, void 0, void 0, function* () {
        const headerToken = request.headers.authorization;
        try {
            const secret = yield (0, ValidAccessToken_1.ValidAccessToken)(headerToken);
            return secret;
        }
        catch (error) {
            throw new Error("Credentials invalid!");
        }
    });
}
exports.validateTokenAccess = validateTokenAccess;
//# sourceMappingURL=Auth.js.map