"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ControllerCustomer = void 0;
const ActionsCustomer_1 = require("../../application/use_cases/Customer/ActionsCustomer");
class ControllerCustomer extends ActionsCustomer_1.ActionsCustomer {
    constructor() {
        super();
    }
    create(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            const actions = new ActionsCustomer_1.ActionsCustomer();
            const createResponse = yield actions.create(payload);
            return createResponse;
        });
    }
    list() {
        return __awaiter(this, void 0, void 0, function* () {
            const actions = new ActionsCustomer_1.ActionsCustomer();
            const listResponse = yield actions.list();
            return listResponse;
        });
    }
    find(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const actions = new ActionsCustomer_1.ActionsCustomer();
            const filterResponse = yield actions.find(filter);
            return filterResponse;
        });
    }
    update(name, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const actions = new ActionsCustomer_1.ActionsCustomer();
            const updateResponse = yield actions.update(id, name);
            return updateResponse;
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const actions = new ActionsCustomer_1.ActionsCustomer();
            const deleteResponse = yield actions.delete(id);
            return deleteResponse;
        });
    }
    search(name, details) {
        return __awaiter(this, void 0, void 0, function* () {
            const actions = new ActionsCustomer_1.ActionsCustomer();
            const search = yield actions.search(name, details);
            return search;
        });
    }
}
exports.ControllerCustomer = ControllerCustomer;
//# sourceMappingURL=Customer.js.map