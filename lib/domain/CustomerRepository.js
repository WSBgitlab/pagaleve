"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerRepository = void 0;
class CustomerRepository {
    // eslint-disable-next-line
    create(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            throw new Error("ERROR CREATE CUSTOMER REPOSITORY");
        });
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    update(name, id, details) {
        return __awaiter(this, void 0, void 0, function* () {
            throw new Error("ERROR UPDATE CUSTOMER REPOSITORY");
        });
    }
    list() {
        return __awaiter(this, void 0, void 0, function* () {
            throw new Error(`ERROR LIST CUSTOMER REPOSITORY`);
        });
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    find(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            throw new Error("ERROR FIND CUSTOMER REPOSITORY");
        });
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    delete(name, id) {
        return __awaiter(this, void 0, void 0, function* () {
            throw new Error("ERROR FIND CUSTOMER REPOSITORY");
        });
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    search(name, details) {
        return __awaiter(this, void 0, void 0, function* () {
            throw new Error("ERROR SEARCH CUSTOMER REPOSITORY");
        });
    }
}
exports.CustomerRepository = CustomerRepository;
//# sourceMappingURL=CustomerRepository.js.map