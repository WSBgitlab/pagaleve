"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Customer = void 0;
class Customer {
    // more fields when necessary for persist in database
    // I prefer simple object for test
    constructor(id = "", name = "", details = "") {
        this.id = id;
        this.details = details;
        this.name = name;
    }
}
exports.Customer = Customer;
//# sourceMappingURL=Customer.js.map