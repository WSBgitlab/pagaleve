"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Server_1 = require("./Server");
// Após resolução das configurações iniciar o servidor
(0, Server_1.init)().then(() => (0, Server_1.start)());
//# sourceMappingURL=Main.js.map