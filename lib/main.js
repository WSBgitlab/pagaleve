"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./server");
// Após resolução das configurações iniciar o servidor
(0, server_1.init)().then(() => (0, server_1.start)());
//# sourceMappingURL=main.js.map